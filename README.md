# Rust Examples

This repo contains some practice projects, learning, examples, notes, reminders
I've put together while learning Rust. Nothing fancy, nothing exciting.

# Cool Stuff

Build docs for all dependencies in a project and open them in the browser:

`cargo doc --open`