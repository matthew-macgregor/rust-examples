# Guessing Game

This is my take on the guessing game from the Rust Book:

https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html

You can compete against the computer and see if your human intellect can best
the computer's algorithm (probably not).