#include <stdio.h>
#include "rustlib.h"

/*
* Naturally, you'll need to do a `cargo build` before this will link!
*
* Compile on Windows (MSVC):
* cl .\rust-with-your-c\src\main.c .\target\debug\rust_with_your_c.dll.lib /link /out:.\target\debug\rust-with-your-c.exe
* 
* Compile on macOS (clang):
* clang src/main.c -L../target/debug -lrust_with_your_c -o main.macos
*/

int main(int argc, char **argv) {
    printf("Hello World!\n");
    rust_function();
}