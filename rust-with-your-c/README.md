# Rust with your C

This example shows the ability to compile and link rust code and use it from
C. Unlike most of the other examples in this repo, this one will require more
than just a `cargo build`.

### Compiling the C Code

#### Windows / MSVC

```ps1
# Compile on Windows (MSVC):
cl .\rust-with-your-c\src\main.c .\target\debug\rust_with_your_c.dll.lib /link /out:.\target\debug\rust-with-your-c.exe
```

#### macOS / clang

```sh
# Compile on macOS (clang):
clang src/main.c -L../target/debug -lrust_with_your_c -o main.macos
```

#### Linux / gcc

TBD - but should be basically the same as macOS / clang above.